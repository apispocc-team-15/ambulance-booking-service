package com.ApiSpoc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ApiSpoc.exceptions.UserNotFoundException;
import com.ApiSpoc.models.entities.CustomResponse;
import com.ApiSpoc.models.entities.User;
import com.ApiSpoc.models.responses.AuthenticationResponse;
import com.ApiSpoc.services.LogoutService;
import com.ApiSpoc.services.UserService;
import com.ApiSpoc.services.impl.AmbulanceServiceImpl;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/auth/user")
public class UserController {
	@Autowired
	AmbulanceServiceImpl ambservice;

	@Autowired
	UserService userService;

	@Autowired
	private LogoutService logoutservice;

	@GetMapping("/logout")
	public ResponseEntity<AuthenticationResponse> logout(
			@NonNull HttpServletRequest request,
			@NonNull HttpServletResponse response)
	{
		
		logoutservice.logout(request, response, null);
		return new ResponseEntity<AuthenticationResponse>(HttpStatus.ACCEPTED);
	}
//	@GetMapping("/foruser")
//	@Secured("ROLE_USER")
//	public ResponseEntity<String> sayHelloFromUser(){
//		return new ResponseEntity<String>("Hello from User secured End point", HttpStatus.OK);
//	}
//	@GetMapping("/foradmin")
//	@Secured("ROLE_ADMIN")
//	public ResponseEntity<String> sayHelloFromAdmin(){
//		return new ResponseEntity<String>("Hello from Admin secured End point", HttpStatus.OK);
//	}
  
	@GetMapping("/hello")
	public ResponseEntity<String> sayHello(@NonNull HttpServletRequest request){
		if(request.getAttribute("expired") != null){
            String err=(String.valueOf(request.getAttribute("expired")));
        }
		
		
		return new ResponseEntity<String>("Hello from secured End point", HttpStatus.OK);
	}
	
	@GetMapping("/foruser")
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity<String> sayHelloFromUser(){
		SecurityContext securityContext = SecurityContextHolder.getContext();
	    System.out.println(securityContext.getAuthentication().getAuthorities()); 
		return new ResponseEntity<String>("Hello from User secured End point", HttpStatus.OK);
	}
	@GetMapping("/foradmin")
	@PreAuthorize("hasAnyRole('ROLE_ADMIN')")
	public ResponseEntity<String> sayHelloFromAdmin(){
		SecurityContext securityContext = SecurityContextHolder.getContext();
	    System.out.println(securityContext.getAuthentication().getAuthorities()); 
		return new ResponseEntity<String>("Hello from Admin secured End point", HttpStatus.OK);
	}
  
	@PostMapping("/editProfile")
	// @PreAuthorize("hasRole('ROLE_VIEWER')")
	public ResponseEntity<CustomResponse> editUserProfile(@RequestBody User user, @RequestParam(value="userId",required = true) Integer userId ) throws UserNotFoundException{
		CustomResponse response =userService.editUserProfile(user, userId);
		return new ResponseEntity<CustomResponse>(response,response.getStatus());

	}
//	@GetMapping("/getAmbulance/{location}")
//	public ResponseEntity<String> sayHello(@PathVariable("location") String location)
//			throws IOException, InterruptedException, ParseException {
//		// ParkingStation p= ambservice.getNearestParkingStation(location);
//		return new ResponseEntity<String>("Hello from secured End point", HttpStatus.OK);
//	}

@GetMapping("/getUser")
public ResponseEntity<User> getUserById(@RequestParam int userId) throws UserNotFoundException{	
	return new ResponseEntity<User>(userService.getUserById(userId),HttpStatus.OK);
}
	

}
