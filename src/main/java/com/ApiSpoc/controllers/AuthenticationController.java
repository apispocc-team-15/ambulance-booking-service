package com.ApiSpoc.controllers;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ApiSpoc.exceptions.UserNotActivatedException;
import com.ApiSpoc.models.entities.Hospital;
import com.ApiSpoc.models.entities.User;
import com.ApiSpoc.models.requests.AuthenticationRequest;
import com.ApiSpoc.models.requests.RegisterRequest;
import com.ApiSpoc.models.responses.AuthenticationResponse;
import com.ApiSpoc.services.AuthenticationService;
import com.ApiSpoc.services.HospitalService;
import com.ApiSpoc.services.UserService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
@CrossOrigin(origins ="http://localhost:3000")
@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationController {
	
	@Autowired
  private  AuthenticationService service;

  @Autowired
  private HospitalService hospitalservice;
  
  @Autowired
  UserService userservice;
	

  @PostMapping("/register")
  public ResponseEntity<AuthenticationResponse> register(
      @RequestBody @Valid RegisterRequest request
  ) {
    return ResponseEntity.ok(service.register(request));
  }
 
  @GetMapping("/activate/{activationCode}")
  public ResponseEntity<String> activateUser(@PathVariable String activationCode) {
      return ResponseEntity.ok(userservice.activateUser(activationCode));
  }
  @PostMapping("/authenticate")
  public ResponseEntity<AuthenticationResponse>  authenticate(
      @RequestBody @Valid AuthenticationRequest request
  ) throws UserNotActivatedException {
    return ResponseEntity.ok(service.authenticate(request));
  }
  @GetMapping("/checkEmail/{email}")
  public ResponseEntity<Boolean> checkEmail(@PathVariable String email) {
      return  ResponseEntity.ok(userservice.emailAlreadyExist(email));
  }

  @PostMapping("/registerHospital")
  public ResponseEntity<Hospital> registerHospital(@RequestBody @Valid Hospital request) {
	  User user=request.getUser();
	  RegisterRequest userReg=new RegisterRequest();
	  userReg.setEmail(user.getEmail());
	  userReg.setFirstname(user.getFirstname());
	  userReg.setLastname(user.getLastname());
	  userReg.setPassword(user.getPassword());
	  userReg.setRole(user.getRole());
	  AuthenticationResponse res=service.register(userReg);
	  int uid=userservice.getUserIdByToken(res.getToken());
	  request.getUser().setId(uid);
	   
    return ResponseEntity.ok(hospitalservice.registerHospital(request));
  }

}
