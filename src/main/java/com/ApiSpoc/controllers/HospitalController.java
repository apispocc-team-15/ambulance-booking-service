package com.ApiSpoc.controllers;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ApiSpoc.exceptions.AddressNotFoundException;
import com.ApiSpoc.exceptions.BadRequestException;
import com.ApiSpoc.exceptions.HospitalNotFoundException;
import com.ApiSpoc.models.entities.Ambulance;
import com.ApiSpoc.models.entities.CustomResponse;
import com.ApiSpoc.models.entities.Hospital;
import com.ApiSpoc.services.AmbulanceService;
import com.ApiSpoc.services.HospitalService;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
@CrossOrigin(origins ="http://localhost:3000")
@RestController
@RequestMapping("/api/v1/auth/hospital")
public class HospitalController {
  @Autowired
  HospitalService hospitalService;

  @Autowired
  AmbulanceService ambulanceService;

  @PostMapping("/editProfile")
  public ResponseEntity<CustomResponse> editUserProfile(@RequestBody Hospital hospital,
      @RequestParam(value = "hospId", required = true) String hospId) throws HospitalNotFoundException {
      CustomResponse response = hospitalService.editHospitalProfile(hospital, hospId);
		return new ResponseEntity<CustomResponse>(response,response.getStatus());  
  }
  
  @PostMapping("/getAmbulaceStatus")
  public ResponseEntity< List<Hospital>> getAmbulaceStatus(
		  @RequestBody (required=false) LatLng location, @RequestParam(required=false) String address) throws Exception {
	  List<Hospital> h=null;
	  if(null!=address) {
	   h=hospitalService.getNearestHospital(address);
	  }
	  else if(null!=location)
	  {
		  h=hospitalService.getNearestHospital(location);
	  }
	  else if(null==address && null==location) {
		  throw new BadRequestException("No Locations Found");
	  }
      return new ResponseEntity< List<Hospital>>(h,HttpStatus.OK);
  }
  


  @GetMapping("/getHospitals")
  public ResponseEntity<List<Hospital>> getHospitals(@RequestParam(required=false) Double lat, @RequestParam(required=false) Double lng,@RequestParam int radiusInMetres ) throws HospitalNotFoundException, AddressNotFoundException, ApiException, InterruptedException, IOException {
    
	LatLng location = new LatLng(lat, lng);
    return new ResponseEntity<List<Hospital>>(hospitalService.getHospitals(location, null,radiusInMetres), HttpStatus.OK);
  }

  @PostMapping("/addAllAmbulanceDetails")
  public ResponseEntity<List<Ambulance>> addAllAmbulanceDetails(@RequestBody List<Ambulance> ambulance) {
      System.out.println("hello.. ambulance");
    return ResponseEntity.ok(ambulanceService.addAllAmbulanceDetails(ambulance));
  }

  @PostMapping("/addAnAmbulance")
  public ResponseEntity<Ambulance> addAnAmbulance(@RequestBody @jakarta.validation.Valid Ambulance ambulance) {
      System.out.println("hello.. ambulance");
    return ResponseEntity.ok(ambulanceService.addAnAmbulance(ambulance));
  }
    
  @GetMapping("/getHospital/{id}")
  public ResponseEntity<Hospital> getHospitalById(@PathVariable("id") String id) throws BadRequestException
  {
    return new ResponseEntity<Hospital>(hospitalService.getHospitalById(id),HttpStatus.OK);
  }


//  @GetMapping("/getHospitals")
//  public ResponseEntity<List<Map<String,String>>> getHospitals(@RequestBody LatLng location) throws HospitalNotFoundException {
//    // LatLng location = new LatLng(12.939370, 77.583530);
//    return new ResponseEntity<List<Map<String,String>>>(hospitalService.getHospitals(location), HttpStatus.OK);
//  }
}
