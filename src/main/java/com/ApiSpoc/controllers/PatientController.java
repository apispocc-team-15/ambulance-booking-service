package com.ApiSpoc.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ApiSpoc.models.entities.Patient;
import com.ApiSpoc.services.PatientService;
@CrossOrigin(origins ="http://localhost:3000")
@RestController
@RequestMapping("/api/v1/auth/")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @GetMapping("/patients")
    public List<Patient> getAllPatients() {
        return patientService.getAllPatients();
    }

    @PostMapping("/patients")
    public void storePatient(@RequestBody Patient patient) {
        patientService.storePatient(patient);
    }
}
