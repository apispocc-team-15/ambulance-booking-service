package com.ApiSpoc.constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.stereotype.Component;
@Component
public class CommonConstants {
	static Properties properties;
	static {
	ClassLoader loader = Thread.currentThread().getContextClassLoader();
	 properties = new Properties();
	try (InputStream resourceStream = loader.getResourceAsStream("application.properties")) {
	    properties.load(resourceStream);
	} catch (IOException e) {
	    e.printStackTrace();
	}
	}
	
	
	public  final static int RADIUS=Integer.parseInt(properties.getProperty("RADIUS"));
	public static final String API_ROOT_PATH = properties.getProperty("API_ROOT_PATH");
	public static final String GOOGLE_MAP_API_KEY=properties.getProperty("GOOGLE_MAP_API_KEY");

	

}
