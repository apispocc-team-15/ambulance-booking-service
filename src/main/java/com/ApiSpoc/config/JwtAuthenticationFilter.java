package com.ApiSpoc.config;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.ApiSpoc.models.entities.CustomResponse;
import com.ApiSpoc.repositories.TokenRepository;
import com.ApiSpoc.services.JwtService;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

	private final JwtService jwtService;
	private final UserDetailsService userDetailsService;
	private final TokenRepository tokenRepository;

	@Override
	protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response,
			@NonNull FilterChain filterChain) throws ServletException, IOException {
		final String authHeader = request.getHeader("Authorization");
		final String jwt;
		final String userEmail;
		if (authHeader == null || !authHeader.startsWith("Bearer ")) {
			// filterChain.doFilter(request, response);

			if ((request.getRequestURI().startsWith("/api/v1/auth/")
					|| request.getRequestURI().startsWith("/swagger-resources")
					|| request.getRequestURI().startsWith("/swagger-ui")
					|| request.getRequestURI().startsWith("/v3/api-docs"))) {
				filterChain.doFilter(request, response);
				return;
			} else {
				handleNoToken(response);
				return;
			}

		}

		jwt = authHeader.substring(7);

		try {
			userEmail = jwtService.extractUsername(jwt);
			if (userEmail != null && SecurityContextHolder.getContext().getAuthentication() == null) {
				UserDetails userDetails = this.userDetailsService.loadUserByUsername(userEmail);
				var isTokenValid = tokenRepository.findByToken(jwt).map(t -> !t.isExpired() && !t.isRevoked())
						.orElse(false);
				if (jwtService.isTokenValid(jwt, userDetails) && isTokenValid) {
					
				userDetails.getAuthorities();
					
//					Set<SimpleGrantedAuthority> simpleGrantedAuthorities=userDetails.getAuthorities().stream()
//		                    .map(m -> new SimpleGrantedAuthority(m.get("authority")))
//		                    .collect(Collectors.toSet());
//					
					UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails,
							null, userDetails.getAuthorities());
					authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					SecurityContextHolder.getContext().setAuthentication(authToken);
				}

			}

		} catch (ExpiredJwtException e) {
			var storedToken = tokenRepository.findByToken(jwt).orElse(null);
			if (storedToken != null) {
				storedToken.setExpired(true);
				storedToken.setRevoked(true);
				tokenRepository.save(storedToken);
				CustomResponse errorResponse = new CustomResponse("Auth token Expired", HttpStatus.UNAUTHORIZED,
						LocalDateTime.now());
				ObjectMapper om = new ObjectMapper();
				response.setContentType("application/json");
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				response.getWriter().write(om.writeValueAsString(errorResponse));
				return;
			}
		}
		filterChain.doFilter(request, response);

	}

	private void handleNoToken(HttpServletResponse response) throws IOException {
		CustomResponse errorResponse = new CustomResponse("No Auth token found", HttpStatus.BAD_REQUEST,
				LocalDateTime.now());
		ObjectMapper om = new ObjectMapper();
		response.setContentType("application/json");
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		response.getWriter().write(om.writeValueAsString(errorResponse));

	}

}
