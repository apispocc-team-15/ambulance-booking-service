package com.ApiSpoc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ApiSpoc.models.entities.Patient;



@Repository
public interface PatientRepository extends JpaRepository<Patient,Integer> {

}
