package com.ApiSpoc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ApiSpoc.models.entities.Ambulance;

@Repository
public interface AmbulanceRepository extends JpaRepository<Ambulance, Integer> {

}
