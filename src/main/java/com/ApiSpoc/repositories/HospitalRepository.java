package com.ApiSpoc.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ApiSpoc.models.entities.Hospital;
import com.ApiSpoc.models.entities.User;

@Repository
public interface HospitalRepository extends JpaRepository<Hospital, String> {
	@Query(value = """
	      Select  h \s
		      from Hospital h where \s
	      currentlyAvailableAmbulanceNo>0 and availability=true\s
		      """)
	public List<Hospital> getAvailableHospitals();
	List<Hospital> findByUser(User user);

}
