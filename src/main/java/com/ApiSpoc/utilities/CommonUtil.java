package com.ApiSpoc.utilities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.maps.GeoApiContext;

@Service
public class CommonUtil {
	
	  @Autowired
	  GeoApiContext geoApiContext;
	 
	
	
	
	// public double getDistanceFromLatLonInKm(double lat1, double lon1, double lat2, double lon2) {
	//     double R = 6371; // Radius of the earth in km
	//     double dLat = deg2rad(lat2-lat1);  // deg2rad below
	//     double dLon = deg2rad(lon2-lon1);
	//     double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	//                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
	//                Math.sin(dLon/2) * Math.sin(dLon/2);
	//     double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	//     double d = R * c; // Distance in km
	//     return d;
	// }

	// public   double deg2rad(double deg) {
	//     return deg * (Math.PI/180);
	// }

	
//    public  DistanceMatrixElement getDistanceBetweenTwoLocations(String origin, String destination)
//            throws IOException, InterruptedException, ParseException, ApiException {
//        DistanceMatrix response = DistanceMatrixApi
//                .getDistanceMatrix(geoApiContext, new String[] { origin }, new String[] { destination }).await();
//        if (response != null) {
//            return response.rows[0].elements[0];
//        }
//        return null;
//    }
//    
//    public  DistanceMatrixElement getDistanceBetweenTwoLocations(LatLng origin, LatLng destination)
//            throws IOException, InterruptedException, ParseException, ApiException {
//    	DistanceMatrixApiRequest req=new DistanceMatrixApiRequest(geoApiContext);
//    	req.origins(origin);
//    	req.destinations(destination);
//        DistanceMatrix response = req.await();
//        if (response != null) {
//            return response.rows[0].elements[0];
//        }
//        return null;
//    }
    
//    public  PlacesSearchResponse getNearByPlaces(LatLng location,String type,int radius) throws ApiException, InterruptedException, IOException {
//        // LatLng location = new LatLng(12.939370, 77.583530);
//        //int radius = 5000; // in meters
//        
//            PlacesSearchResponse response = PlacesApi.nearbySearchQuery(geoApiContext, location)
//                    .radius(radius)
//                    .keyword(type)
//                    .await();
//            
//            return response;
//    }
    
//    public LatLng getLatLng(String address) throws ApiException, InterruptedException, IOException {
//        GeocodingResult[] result = GeocodingApi.geocode(geoApiContext, address).await();
//        if (result.length > 0) {
//            return result[0].geometry.location;
//        }
//        return null;
//    }

        
        

}
