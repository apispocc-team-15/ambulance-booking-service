package com.ApiSpoc.exceptions;

public class HospitalNotFoundException extends Exception {
    public HospitalNotFoundException(String message) {
        super(message);
    }
}
