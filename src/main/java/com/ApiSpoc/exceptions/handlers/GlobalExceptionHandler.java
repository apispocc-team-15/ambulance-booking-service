package com.ApiSpoc.exceptions.handlers;

import java.time.LocalDateTime;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.ApiSpoc.exceptions.BadRequestException;
import com.ApiSpoc.exceptions.UserNotActivatedException;
import com.ApiSpoc.models.entities.CustomResponse;

import io.jsonwebtoken.ExpiredJwtException;
import javax.servlet.ServletException;
import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(value = UserNotActivatedException.class)
	public ResponseEntity<CustomResponse> userNotActivated(UserNotActivatedException e) {
		
		return new ResponseEntity(new CustomResponse(e.getMessage(), HttpStatus.UNAUTHORIZED, LocalDateTime.now()), HttpStatus.BAD_REQUEST);
	}

	
	@ExceptionHandler(value = MethodArgumentNotValidException.class)
	public ResponseEntity<CustomResponse> argumentsInvalid(MethodArgumentNotValidException e) {
		
		return new ResponseEntity(new CustomResponse(e.getFieldError().getDefaultMessage(), HttpStatus.BAD_REQUEST, LocalDateTime.now()), HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value = ConstraintViolationException.class)
	public ResponseEntity<CustomResponse> argumentsInvalid(ServletException e) {
		CustomResponse error = new CustomResponse("Constraints Violated", HttpStatus.BAD_REQUEST,
				LocalDateTime.now());
		return new ResponseEntity<CustomResponse>(error, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<CustomResponse> argumentsInvalid(DataIntegrityViolationException e) {
		CustomResponse error = new CustomResponse("Duplicate Data Found", HttpStatus.BAD_REQUEST,
				LocalDateTime.now());
		return new ResponseEntity<CustomResponse>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value = BadRequestException.class)
	public ResponseEntity<String> badRequest(BadRequestException e) {
		return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(value = ExpiredJwtException.class)
	public ResponseEntity detailsNotFound(ExpiredJwtException e) {
		
		return new ResponseEntity(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@ExceptionHandler(value = Exception.class)
	public ResponseEntity<CustomResponse> exception(Exception e) {
		CustomResponse error = new CustomResponse(e.getMessage(), HttpStatus.BAD_REQUEST,
				LocalDateTime.now());
		return new ResponseEntity<CustomResponse>(error, HttpStatus.BAD_REQUEST);
	}

}







