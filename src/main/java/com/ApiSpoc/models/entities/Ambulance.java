package com.ApiSpoc.models.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Ambulance {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int ambulanceId;
	@NotBlank(message = "Ambulance Number should not be blank")
	private String ambulanceNo;
	@NotBlank(message = "Registration Number should not be blank")
	private String registrationNo;
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "hospitalId")
	private Hospital hos;
	private Boolean availability;

}
