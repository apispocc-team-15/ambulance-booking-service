package com.ApiSpoc.models.entities;

import java.util.Collection;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "_user")
public class User implements UserDetails {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @NotBlank(message = "First Name should not be blank")
  private String firstname;
  @NotBlank(message = "Last Name should not be blank")
  private String lastname;
  @NotBlank
  @Email(message = "Please enter a valid e-mail address")
  @Column(unique = true)
  private String email;
  @NotBlank
  private String password;
  @Size(min=0,max=10, message = "phone_no should be exact 10 characters.")
  private String phone;
  private String gender;
  private String address;
  private String bloodType;
  @DateTimeFormat(pattern = "yyyy-mm-dd")
  private String dob;
  // @OneToOne
  // @JoinColumn(name = "patientId")
  // private Patient patientId;

  private String activationCode;
  private boolean activated;
  
  @Enumerated(EnumType.STRING)
  private Role role;

  @OneToMany(mappedBy = "user")
  @JsonManagedReference
  private List<Token> tokens;
  
 

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return List.of(new SimpleGrantedAuthority("ROLE_"+role.name()));
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return email;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

//   @JsonManagedReference
//   public List<Token> getTokens(){
//     return this.tokens;
//   }
}
