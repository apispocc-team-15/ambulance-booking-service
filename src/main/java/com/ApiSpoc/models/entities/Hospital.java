package com.ApiSpoc.models.entities;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Hospital {

	@Id
	private String hospitalId;
	@NotBlank(message = "Name should not be blank")
	private String name;
	@NotBlank(message = "Address should not be blank")
	private String address;
	@Size(min=0,max=10, message = "Phone Number should be exact 10 characters.")
	private String phone;
	@NotBlank(message = "Registration Number should not be blank")
	private String registrationNo;
	private int emergenyWardBedCount;
	@Column(name = "icu_bed_count")
	private int ICUBedCount;
	@NotNull(message = "Ambulance Capacity should not be blank")
	private int ambulanceCapacity;
	private int currentlyAvailableAmbulanceNo;
	@JsonManagedReference
	@OneToMany(mappedBy = "hos",cascade = {CascadeType.ALL})
	private List<Ambulance> ambulances;
	private double lat;
	private double lng;
	private boolean availability;
	@OneToOne
	private User user;

}
