package com.ApiSpoc.models.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Driver {
	@Id
	private String driverId;
	private String driverName;
	private int contactNo;
	// @OneToOne
	// @JoinColumn(name = "parkingStation")
	// private ParkingStation parkingStation;
	private Boolean availavility;
}
