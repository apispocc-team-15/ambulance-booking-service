package com.ApiSpoc.models.entities;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AmbulanceBooking {

	@Id
	private String bookingId;
	@OneToOne
	@JoinColumn(name = "ambulanceId")
	private Ambulance ambulanceId;
	@OneToOne
	@JoinColumn(name = "driverId")
	private Driver driverId;
	@OneToOne
	@JoinColumn(name = "userId")
	private User userId;
	private String pickUpAddress;
	@OneToOne
	@JoinColumn(name = "hospitalId")
	private Hospital hospitalId;
	private Date bookingTime;

	private double billAmount;

}
