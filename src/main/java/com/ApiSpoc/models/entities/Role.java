package com.ApiSpoc.models.entities;

public enum Role {

  USER,
  ADMIN,
  HOSPITAL
}
