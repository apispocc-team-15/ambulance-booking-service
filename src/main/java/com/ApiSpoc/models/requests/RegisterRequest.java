package com.ApiSpoc.models.requests;

import com.ApiSpoc.models.entities.Role;
import com.fasterxml.jackson.annotation.JsonProperty;

import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterRequest {

  private String firstname;
  private String lastname;
  private String email;
  private String phone;
  private String password;
  @Enumerated(EnumType.STRING)
  private Role role;
  @JsonProperty("isOAuth")
 @Builder.Default
  private boolean isOAuth=false;
}
