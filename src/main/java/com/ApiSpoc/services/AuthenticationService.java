package com.ApiSpoc.services;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ApiSpoc.exceptions.UserNotActivatedException;
import com.ApiSpoc.models.entities.Token;
import com.ApiSpoc.models.entities.TokenType;
import com.ApiSpoc.models.entities.User;
import com.ApiSpoc.models.requests.AuthenticationRequest;
import com.ApiSpoc.models.requests.RegisterRequest;
import com.ApiSpoc.models.responses.AuthenticationResponse;
import com.ApiSpoc.repositories.TokenRepository;
import com.ApiSpoc.repositories.UserRepository;
import com.ApiSpoc.services.impl.EmailSenderServiceImpl;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AuthenticationService {
	private final UserRepository repository;
	private final TokenRepository tokenRepository;
	private final PasswordEncoder passwordEncoder;
	private final JwtService jwtService;
	private final AuthenticationManager authenticationManager;
	private final EmailSenderServiceImpl emailService;
	// Base URL of your application, e.g., http://localhost:8080

	public AuthenticationResponse register(RegisterRequest request) {

		var user = User.builder().firstname(request.getFirstname()).lastname(request.getLastname())
				.phone(request.getPhone()).email(request.getEmail())
				.password(passwordEncoder.encode(request.getPassword())).role(request.getRole()).activated(false)
				.activationCode(UUID.randomUUID().toString()).build();
		if (request.isOAuth()) {
			if (repository.existsByEmail(user.getEmail())) {
				user.setId((repository.findByEmail(user.getEmail())).get().getId());
			}
			user.setActivated(true);
			user.setActivationCode("");

		}
		var savedUser = repository.save(user);
		if (request.isOAuth()) {
			emailService.sendWelcomeMessage(savedUser);
		} else {
			emailService.sendWelcomeAndActivationCode(savedUser);
		}

		var jwtToken = jwtService.generateToken(user);
		saveUserToken(savedUser, jwtToken);
		return AuthenticationResponse.builder().token(jwtToken).build();
	}

	public AuthenticationResponse authenticate(AuthenticationRequest request) throws UserNotActivatedException {
		authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
		var user = repository.findByEmail(request.getEmail()).orElseThrow();
		if (!user.isActivated()) {
			throw new UserNotActivatedException("User is not Activated");
		}
		var jwtToken = jwtService.generateToken(user);
		revokeAllUserTokens(user);
		saveUserToken(user, jwtToken);
		return AuthenticationResponse.builder().token(jwtToken).build();
	}

	private void saveUserToken(User user, String jwtToken) {
		var token = Token.builder().user(user).token(jwtToken).tokenType(TokenType.BEARER).expired(false).revoked(false)
				.build();
		tokenRepository.save(token);
	}

	private void revokeAllUserTokens(User user) {
		var validUserTokens = tokenRepository.findAllValidTokenByUser(user.getId());
		if (validUserTokens.isEmpty())
			return;
		validUserTokens.forEach(token -> {
			token.setExpired(true);
			token.setRevoked(true);
		});
		tokenRepository.saveAll(validUserTokens);
	}
}
