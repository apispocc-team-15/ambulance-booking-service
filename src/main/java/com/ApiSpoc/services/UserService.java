package com.ApiSpoc.services;

import org.springframework.stereotype.Service;

import com.ApiSpoc.exceptions.UserNotFoundException;
import com.ApiSpoc.models.entities.CustomResponse;
import com.ApiSpoc.models.entities.User;

@Service
public interface UserService {
    CustomResponse editUserProfile(User user, Integer userId) throws UserNotFoundException;
    int getUserIdByToken(String token);
    public String activateUser(String activationCode);
    public boolean emailAlreadyExist(String email);

    User getUserById(int id) throws UserNotFoundException;
}
