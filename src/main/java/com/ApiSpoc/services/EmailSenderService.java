package com.ApiSpoc.services;

import org.springframework.stereotype.Service;

@Service
public interface EmailSenderService {
	
	 public void sendSimpleEmail(String toEmail,
             String subject,
             String body
);

}
