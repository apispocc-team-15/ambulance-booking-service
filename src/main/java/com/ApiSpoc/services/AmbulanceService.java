package com.ApiSpoc.services;

import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;

import com.ApiSpoc.models.entities.Ambulance;

public interface AmbulanceService {

    public double getDistanceBetweenTwoLocations(String origin, String destination) throws IOException, InterruptedException, ParseException;
    public double parse(String response) throws ParseException;
    public Ambulance addAnAmbulance(Ambulance ambulance);
    public List<Ambulance> addAllAmbulanceDetails(List<Ambulance> ambulance);
}
