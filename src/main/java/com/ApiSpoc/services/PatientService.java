package com.ApiSpoc.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ApiSpoc.models.entities.Patient;
import com.ApiSpoc.repositories.PatientRepository;

@Service
public class PatientService {

	 	@Autowired
	    private PatientRepository patientRepository;

	 	public List<Patient> getAllPatients() {
	        return patientRepository.findAll();
	    }
	    public void storePatient(Patient patient) {
	        patientRepository.save(patient);
	    }
}
	

