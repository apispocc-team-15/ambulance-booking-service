package com.ApiSpoc.services;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import com.ApiSpoc.exceptions.AddressNotFoundException;
import com.ApiSpoc.exceptions.BadRequestException;
import com.ApiSpoc.exceptions.HospitalNotFoundException;
import com.ApiSpoc.models.entities.CustomResponse;
import com.ApiSpoc.models.entities.Hospital;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;

@Service
public interface HospitalService {
    CustomResponse editHospitalProfile(Hospital hospital, String hospId) throws HospitalNotFoundException;
    public List<Hospital> getNearestHospital(String origin) throws
    IOException, InterruptedException, ParseException, ApiException;
    List<Hospital> getHospitals(LatLng location, String address, int radiusInMetres) throws AddressNotFoundException, ApiException, InterruptedException, IOException;
    public Hospital registerHospital(Hospital request);
    public  List<Hospital> getNearestHospital(LatLng latlng)
            throws IOException, InterruptedException, ParseException, ApiException ;
    public Hospital getHospitalById(String id) throws BadRequestException;
}

