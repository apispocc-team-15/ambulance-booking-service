package com.ApiSpoc.services.impl;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.ApiSpoc.exceptions.AddressNotFoundException;
import com.ApiSpoc.exceptions.BadRequestException;
import com.ApiSpoc.constants.CommonConstants;
import com.ApiSpoc.exceptions.HospitalNotFoundException;
import com.ApiSpoc.models.entities.Ambulance;
import com.ApiSpoc.models.entities.CustomResponse;
import com.ApiSpoc.models.entities.Hospital;
import com.ApiSpoc.repositories.HospitalRepository;
import com.ApiSpoc.services.HospitalService;
import com.ApiSpoc.utilities.CommonUtil;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.PlacesApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DistanceMatrix;
import com.google.maps.model.DistanceMatrixElement;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.PlacesSearchResponse;
import com.google.maps.model.PlacesSearchResult;

@Service
public class HospitalServiceImpl implements HospitalService {
    @Autowired
    GeoApiContext geoApiContext;

    @Autowired
    HospitalRepository hospitalRepository;

    @Override
    public CustomResponse editHospitalProfile(Hospital hospital, String hospId) throws HospitalNotFoundException {
        return hospitalRepository.findById(hospId).map(fetchedHospital -> {
            fetchedHospital.setAddress(
                    hospital.getAddress() == null || hospital.getAddress() == "" ? fetchedHospital.getAddress()
                            : hospital.getAddress());
            fetchedHospital.setEmergenyWardBedCount(
                    hospital.getEmergenyWardBedCount() == 0 ? fetchedHospital.getEmergenyWardBedCount()
                            : hospital.getEmergenyWardBedCount());
            fetchedHospital.setICUBedCount(
                    hospital.getICUBedCount() == 0 ? fetchedHospital.getICUBedCount() : hospital.getICUBedCount());
            fetchedHospital
                    .setPhone(hospital.getPhone() == null || hospital.getPhone() == "" ? fetchedHospital.getPhone()
                            : hospital.getPhone());
            if (! hospitalRepository.save(fetchedHospital).equals(null)) {
                return new CustomResponse("Hospital Profile Edited Successfully", HttpStatus.OK, LocalDateTime.now());
            } else {
                return new CustomResponse("Hospital Profile Edit Failed", HttpStatus.BAD_REQUEST, LocalDateTime.now());

            }
        }).orElseThrow(() -> new HospitalNotFoundException("No Hospital Found"));

    }

    // public Hospital getNearestHospital(String origin) throws
    // IOException, InterruptedException, ParseException
    // {
    // List<Hospital> hospitals=new ArrayList<>();
    // //get Lat and Lng fromm the address
    // //Get List of nearby Hospitals from google nearbyAPI
    // hospitals=hospitalRepository.getAvailableHospitals();
    // Map<Hospital,Double> distanceMap=new HashMap<>();
    // double mindistance=Double.MAX_VALUE;
    // Hospital nearestHospital=null;
    // for(Hospital h:hospitals)
    // {
    // double d=getDistanceBetweenTwoLocations(origin, h.getAddress());
    // if(d<mindistance)
    // {
    // mindistance=d;
    // nearestHospital=h;
    //
    // List<Hospital> sortedHosList=sortByValue(distanceMap);
    //
    // return nearestHospital;
    // }
    // }
    // }

    @Override
    public List<Hospital> getHospitals(LatLng location, String address, int radiusInMetres)
            throws AddressNotFoundException, ApiException, InterruptedException, IOException {
        if (location == null && address.equals(null)) {
            throw new AddressNotFoundException("Address details not passed");
        } else if (location == null && !address.equals(null)) {
            location = getLatLng(address);
        }
        // LatLng location = new LatLng(12.939370, 77.583530);
        List<Hospital> hospitalList = new ArrayList<Hospital>();
        // int radius = 5000; // in meters
        try {
            PlacesSearchResponse response = PlacesApi.nearbySearchQuery(geoApiContext, location)
                    .radius(radiusInMetres)
                    .keyword("hospital")
                    .await();
            for (PlacesSearchResult result : response.results) {
                // Map<String, Object> hospital = new HashMap<>();
                System.out.println(result);
                if((hospitalRepository.findById(result.placeId)).isPresent())
                {
                	continue;
                }
                Hospital hospital = new Hospital();
                hospital.setAddress(result.vicinity);
                hospital.setName(result.name);
             
                hospital.setHospitalId(result.placeId);
                hospital.setLat(result.geometry.location.lat);
                hospital.setLng(result.geometry.location.lng);
                hospital.setPhone(String.valueOf((int) (Math.random() * 10000000000.0)));
               
                List<Ambulance> ambulances = new ArrayList<>();
                for (int i = 1; i <= (int) (Math.random() * 10); i++) {
                    Ambulance ambulance = new Ambulance();
                    ambulance.setRegistrationNo("KA 37 R " + (hospitalList.size() + 1) + i);
                    ambulance.setAmbulanceNo("KA 37 R " + (hospitalList.size() + 1) + i);
                    ambulance.setHos(hospital);
                    ambulance.setAvailability(true);
                    ambulances.add(ambulance);
                }
                hospital.setAmbulances(ambulances);
                hospital.setAmbulanceCapacity(ambulances.size());
                hospital.setEmergenyWardBedCount((int) (Math.random() * 100));
                hospital.setICUBedCount((int) (Math.random() * 100));
                hospital.setCurrentlyAvailableAmbulanceNo(ambulances.size());
                hospital.setAvailability(true);
                hospital.setRegistrationNo("WBMC9001");
                hospitalList.add(hospital);
            }
            //hospitalRepository.saveAll(hospitalList);
            return hospitalList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Hospital> getNearestHospital(String origin)
            throws IOException, InterruptedException, ParseException, ApiException {
        List<Hospital> allhospitals = new ArrayList<>();
        LatLng latlng = getLatLng(origin);

        allhospitals = hospitalRepository.getAvailableHospitals();
        if (allhospitals.isEmpty())
            return null;
        List<Hospital> filteredHos = new ArrayList<>();
        for (Hospital h : allhospitals) {
            if (getDistanceFromLatLonInKm(latlng.lat, latlng.lng, h.getLat(), h.getLng()) <= 10) {
                filteredHos.add(h);
            }
        }
        // System.out.println(filteredHos);
        Map<Hospital, Double> distanceMap = new HashMap<>();
        double mindistance = Double.MAX_VALUE;
        Hospital nearestHospital = null;
        for (Hospital h : filteredHos) {
            DistanceMatrixElement distanceMatrixElement = getDistanceBetweenTwoLocations(origin, h.getAddress());
            double d = Double.parseDouble(distanceMatrixElement.distance.humanReadable.split(" ")[0]);
            if (d < mindistance) {
                mindistance = d;
                nearestHospital = h;
            }
            distanceMap.put(h, d);
        }

        return sortByValue(distanceMap);
    }

    public List<Hospital> getNearestHospital(LatLng latlng)
            throws IOException, InterruptedException, ParseException, ApiException {
        List<Hospital> allhospitals = new ArrayList<>();

        allhospitals = hospitalRepository.getAvailableHospitals();
        List<Hospital> filteredHos = new ArrayList<>();
        for (Hospital h : allhospitals) {
            if (getDistanceFromLatLonInKm(latlng.lat, latlng.lng, h.getLat(), h.getLng()) <= CommonConstants.RADIUS) {
                filteredHos.add(h);
            }
        }
        System.out.println(filteredHos);
        Map<Hospital, Double> distanceMap = new HashMap<>();
        double mindistance = Double.MAX_VALUE;
        Hospital nearestHospital = null;
        for (Hospital h : filteredHos) {
            DistanceMatrixElement distanceMatrixElement = getDistanceBetweenTwoLocations(latlng,
                    new LatLng(h.getLat(), h.getLng()));
            double d = Double.parseDouble(distanceMatrixElement.distance.humanReadable.split(" ")[0]);
            if (d < mindistance) {
                mindistance = d;
                nearestHospital = h;
            }
            distanceMap.put(h, d);
        }

        return sortByValue(distanceMap);
    }

    public List<Map<String, String>> getHospitals(LatLng location)
            throws ApiException, InterruptedException, IOException {
        // LatLng location = new LatLng(12.939370, 77.583530);
        List<Map<String, String>> hospitalList = new ArrayList<Map<String, String>>();
        // in meters

        PlacesSearchResponse response = getNearByPlaces(location, "HOSPITAL", CommonConstants.RADIUS);
        for (PlacesSearchResult result : response.results) {
            Map<String, String> hospital = new HashMap<String, String>();
            hospital.put("name", result.name);
            hospital.put("address", result.vicinity);
            // getDistanceBetweenTwoLocations(result.vicinity, "Glaze Dry Cleaners, TC
            // Palya, Akshay Nagar 2nd Block");
            // System.out.println(getLatLng(result.vicinity));
            hospitalList.add(hospital);
        }
        return hospitalList;
    }

    public List<Hospital> sortByValue(Map<Hospital, Double> distanceMap) {

        ArrayList list = new ArrayList();
        for (Map.Entry<Hospital, Double> entry : distanceMap.entrySet()) {
            list.add(entry);
        }

        Collections.sort(list, new Comparator<Map.Entry<Hospital, Double>>() {
            @Override
            public int compare(Map.Entry<Hospital, Double> o1, Map.Entry<Hospital, Double> o2) {
                return ((Map.Entry<Hospital, Double>) o1).getValue()
                        .compareTo(((Map.Entry<Hospital, Double>) o2).getValue());
            }
        });
        Object[] sortedList = list.toArray();
        List<Hospital> sortedHos = new ArrayList<>();
        for (int i = 0; i < sortedList.length; i++) {
            Map.Entry<Hospital, Double> entry = (Map.Entry<Hospital, Double>) sortedList[i];
            sortedHos.add(entry.getKey());
        }
        return sortedHos;

    }

    public LatLng getLatLng(String address) throws ApiException, InterruptedException, IOException {
        GeocodingResult[] result = GeocodingApi.geocode(geoApiContext, address).await();
        if (result.length > 0) {
            return result[0].geometry.location;
        }
        return null;
    }

    public double getDistanceFromLatLonInKm(double lat1, double lon1, double lat2, double lon2) {
        double R = 6371; // Radius of the earth in km
        double dLat = deg2rad(lat2 - lat1); // deg2rad below
        double dLon = deg2rad(lon2 - lon1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c; // Distance in km
        return d;
    }

    public double deg2rad(double deg) {
        return deg * (Math.PI / 180);
    }

    public DistanceMatrixElement getDistanceBetweenTwoLocations(String origin, String destination)
            throws IOException, InterruptedException, ParseException, ApiException {
        DistanceMatrix response = DistanceMatrixApi
                .getDistanceMatrix(geoApiContext, new String[] { origin }, new String[] { destination }).await();
        if (response != null) {
            return response.rows[0].elements[0];
        }
        return null;
    }

    public DistanceMatrixElement getDistanceBetweenTwoLocations(LatLng origin, LatLng destination)
            throws IOException, InterruptedException, ParseException, ApiException {
        DistanceMatrixApiRequest req = new DistanceMatrixApiRequest(geoApiContext);
        req.origins(origin);
        req.destinations(destination);
        DistanceMatrix response = req.await();
        if (response != null) {
            return response.rows[0].elements[0];
        }
        return null;
    }

    public PlacesSearchResponse getNearByPlaces(LatLng location, String type, int radius)
            throws ApiException, InterruptedException, IOException {
        PlacesSearchResponse response = PlacesApi.nearbySearchQuery(geoApiContext, location)
                .radius(radius)
                .keyword(type)
                .await();

        return response;
    }

    @Override
    public Hospital registerHospital(Hospital request) {
        // TODO Auto-generated method stub
        return hospitalRepository.save(request);
    }

    @Override
    public Hospital getHospitalById(String id) throws BadRequestException 
    {
      
        Optional<Hospital> res= hospitalRepository.findById(id);
        if(!res.isPresent())
			{
				throw new BadRequestException("Searched Hospital not Found");
			}
			else
				return res.get();
    }

}
