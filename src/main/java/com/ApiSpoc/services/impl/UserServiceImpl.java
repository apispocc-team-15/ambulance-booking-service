package com.ApiSpoc.services.impl;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ApiSpoc.exceptions.UserNotFoundException;
import com.ApiSpoc.models.entities.CustomResponse;
import com.ApiSpoc.models.entities.User;
import com.ApiSpoc.repositories.TokenRepository;
import com.ApiSpoc.repositories.UserRepository;
import com.ApiSpoc.services.UserService;
import com.ApiSpoc.models.entities.Token;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final PasswordEncoder passwordEncoder;
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    TokenRepository tokenrepo;

    @Override
    public CustomResponse editUserProfile(User user, Integer userId) throws UserNotFoundException {
        System.out.println("HMM" + userId);
        return userRepository.findById(userId).map(fetchedUser -> {
            fetchedUser.setBloodType(user.getBloodType()==null ||user.getBloodType()==""? fetchedUser.getBloodType():user.getBloodType());
            fetchedUser.setAddress(user.getAddress()==null ||user.getAddress()==""? fetchedUser.getAddress():user.getAddress());
            fetchedUser.setDob(user.getDob()==null ||user.getDob()==""? fetchedUser.getDob():user.getDob());
            // fetchedUser.setEmail(fetchedUser.getEmail());
            fetchedUser.setFirstname(user.getFirstname()==null ||user.getFirstname()==""? fetchedUser.getFirstname():user.getFirstname());
            fetchedUser.setLastname(user.getLastname()==null ||user.getLastname()==""? fetchedUser.getLastname():user.getLastname());
            fetchedUser.setGender(user.getGender()==null ||user.getGender()==""? fetchedUser.getGender():user.getGender());
            fetchedUser.setPassword(user.getPassword()==null ||user.getPassword()==""? fetchedUser.getPassword():passwordEncoder.encode(user.getPassword()));
            // fetchedUser.setPatientId(user.getPatientId());
            fetchedUser.setPhone(user.getPhone().length()==0? fetchedUser.getPhone():user.getPhone());
            fetchedUser.setRole(user.getRole()==null? fetchedUser.getRole():user.getRole());
            // fetchedUser.setTokens(user.getTokens()==null? fetchedUser.getTokens():user.getTokens());
            if(!userRepository.save(fetchedUser).equals(null)){
                return new CustomResponse("User Profile Edited Successfully", HttpStatus.OK, LocalDateTime.now());
            }
            else{
                return new CustomResponse("User Profile Edit Failed", HttpStatus.BAD_REQUEST, LocalDateTime.now());
 
            }
        }).orElseThrow(() -> new UserNotFoundException("No User Found"));

    }

	@Override
	public int getUserIdByToken(String token) {
		// TODO Auto-generated method stub
		
		Optional<Token> res=tokenrepo.findByToken(token);
		if(res.isPresent())
		{
			Token t=res.get();
			User u=t.getUser();
			return u.getId();
		}
		return 0;
	}
	 public String activateUser(String activationCode) {
	        Optional<User> user = userRepository.findByActivationCode(activationCode);
	        if (user.isPresent()) {
	            user.get().setActivated(true);
	            user.get().setActivationCode(null);
	            userRepository.save(user.get());
	            return "User Activated Sucessfully";
	        }
	        else
	        	return "Invalid Link";
	    }

	@Override
	public boolean emailAlreadyExist(String email) {
		return userRepository.existsByEmail(email);
	}
    @Override
    public User getUserById(int userId) throws UserNotFoundException {
        Optional<User> user = userRepository.findById(userId);
        if(user.isPresent()){
            return user.get();
        }
        else{
            throw new UserNotFoundException("User does not exist");
        }
    }

}
