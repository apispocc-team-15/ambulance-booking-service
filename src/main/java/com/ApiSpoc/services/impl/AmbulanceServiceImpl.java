package com.ApiSpoc.services.impl;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ApiSpoc.constants.CommonConstants;
import com.ApiSpoc.models.entities.Ambulance;
import com.ApiSpoc.repositories.AmbulanceRepository;
import com.ApiSpoc.services.AmbulanceService;

@Service
public class AmbulanceServiceImpl implements AmbulanceService{

    @Autowired
    AmbulanceRepository ambulanceRepository;


    public double getDistanceBetweenTwoLocations(String origin, String destination)
            throws IOException, InterruptedException, ParseException {
        var url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin + "&destinations="
                + destination + "&units=imperial&key=" + CommonConstants.GOOGLE_MAP_API_KEY;
        url = url.replaceAll(" ", "%20");
        var request = HttpRequest.newBuilder().GET().uri(URI.create(url)).build();
        var client = HttpClient.newBuilder().build();
        var response = client.send(request, HttpResponse.BodyHandlers.ofString()).body();
        System.out.println(response);

        return parse(response);

    }

    public double parse(String response) throws ParseException {
        long distance = -1L;
        long time = -1L;
        // parsing json data and updating data
        {

            JSONParser jp = new JSONParser();
            JSONObject jo = (JSONObject) jp.parse(response);
            JSONArray ja = (JSONArray) jo.get("rows");
            jo = (JSONObject) ja.get(0);
            ja = (JSONArray) jo.get("elements");
            jo = (JSONObject) ja.get(0);
            JSONObject je = (JSONObject) jo.get("distance");
            JSONObject jf = (JSONObject) jo.get("duration");
            distance = (long) je.get("value");
            time = (long) jf.get("value");

            return distance;
        }
    }

   

    public Ambulance addAnAmbulance(Ambulance ambulance){
        return ambulanceRepository.save(ambulance);
    }

    public List<Ambulance> addAllAmbulanceDetails(List<Ambulance> ambulance){
        List<Ambulance> ambList= ambulanceRepository.saveAll(ambulance);
        return ambList;
    }

}
