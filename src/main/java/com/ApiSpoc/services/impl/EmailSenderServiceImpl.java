package com.ApiSpoc.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.ApiSpoc.models.entities.User;
import com.ApiSpoc.services.EmailSenderService;

@Service
public class EmailSenderServiceImpl implements EmailSenderService {
	@Autowired
	private JavaMailSender mailSender;

	@Value("${app.base.url}")
	private String baseUrl;

	public void sendSimpleEmail(String toEmail, String subject, String body) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom("info@ambulancebooking.com");
		message.setTo(toEmail);
		message.setText(body);
		message.setSubject(subject);
		mailSender.send(message);

	}

	public void sendWelcomeAndActivationCode(User user) {
		String activationLink = baseUrl + "/api/v1/auth/activate/" + user.getActivationCode();
		sendSimpleEmail(user.getEmail(), "Welcome to Ambulance Booking Management System",
				"Hello " + user.getFirstname() + ",\n" + "Welcome to Ambulance Booking Management System.\n"
						+ "We are happy to have you onboard.\n"
						+ "Please click on the following link to activate the account. \n" + activationLink
						+ "\n Regards,\n" + "Team Ambulance Booking System");

	}

	public void sendWelcomeMessage(User user) {

		sendSimpleEmail(user.getEmail(), "Welcome to Ambulance Booking Management System",
				"Hello " + user.getFirstname() + ",\n" + "Welcome to Ambulance Booking Management System.\n"
						+ "We are happy to have you onboard.\n");

	}

}
